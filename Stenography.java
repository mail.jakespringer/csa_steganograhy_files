import java.awt.Color;

public class Stenography {

    public static void main(String[] args)
    {

    }

    /** 
     * A method that find the differences between two images
     * @param i1
     * @param i2
     */
    public static ArrayList<point> findDifferences(Picture i1, Picture i2)
    {
        difs = new ArrayList<point>();

        if(!sameSize(i1, i2)) return difs;

        Pixel[][] p1 = i1.getPixels2D();
        Pixel[][] p2 = i2.getPixels2D();

        for (int r = x; r < secret.length + x; r++)
        {
            for (int c = y; c < secret[0].length + y; c++)
            {
                Color col1 = p1[r][c].getColor();
                Color col2 = p2[r][c].getColor();
                if(!col1.equals(col2))
                {
                    difs.add(new Point(r,c));
                }
            }
        }

        return difs
    }
        
    /**
     * A method that checks if two images are the same.
     * @param i1
     * @param i2
     */
    public static boolean isSame(Picture i1, Picture i2)
    {
        if(!sameSize(i1, i2)) return false;

        Pixel[][] p1 = i1.getPixels2D();
        Pixel[][] p2 = i2.getPixels2D();

        for (int r = x; r < secret.length + x; r++)
        {
            for (int c = y; c < secret[0].length + y; c++)
            {
                Color col1 = p1[r][c].getColor();
                Color col2 = p2[r][c].getColor();
                if(!col1.equals(col2))
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Determines whether secret can be hidden in source, which is
     * true if secret is less than or equal to the sources dimensions.
     * @param source is not null
     * @param secret is not null
     * @return true if secret can be hidden in source, false otherwise.
     */
    public static boolean canHide(Picture source, Picture secret)
    {
        return (source.getWidth() >= secret.getWidth()) 
            && (source.getHeight() >= secret.getHeight());
    }

    /**
     * Checks if two images are the same size
     * @param i1
     * @param i2
     */
    public static boolean sameSize(Picture i1, Picture i2)
    {
        return (i1.getWidth() == i2.getWidth())
            && (i1.getHeight() == i2.getHeight());
    }

    /**
     * Sets the highest two bits of each pixel's colors 
     * in the source image to the lowest two bits of each 
     * pixels's color in the hidden one.
     * @param hidden
     */
    public static Picture revealPicture(Picture hidden)
    {
        Picture copy = new Picture(hidden);

        Pixel[][] pixels = copy.getPixels2D();
        Pixel[][] source = hidden.getPixels2D();

        for (int r = 0; r < pixels.length; r++)
        {
            for (int c = 0; c < pixels[0].length; c++)
            {
                Color col = source[r][c].getColor();
                getLow(pixels[r][c]);
            }
        }

        return copy;
    }

    /** 
     * Creates a new Picture with data from secret hidden in data from source
     * @param source is not null
     * @param secret is not null
     * @return combined picture with secret hidden in source
     * precondition: source is same width and height as secret
     */
    public static Picture hidePicture(Picture source, Picture secret, int startRow, int startColumn)
    {
        if(!canHide(source, secret)) return null;

        x = startRow;
        y = startColumn;

        Picture copy = new Picture(source);

        Pixel[][] pixels = copy.getPixels2D();
        Pixel[][] secret = secret.getPixels2D();

        for (int r = x; r < secret.length + x; r++)
        {
            for (int c = y; c < secret[0].length + y; c++)
            {
                Color col = secret[r][c].getColor();
                Pixel pixel = pixels[r][c];
                setLow(pixel, col);
            }
        }

        return copy;
    }

    /** 
     * Makes the default start position (0, 0)
     */
    public static Picture hidePicture(Picture source, Picture secret)
    {
        return hidePicture(source, secret, 0, 0);
    }

    /** 
     * Test for the clearLow function with a test image
     * @param img
     */
    public static Picture testClearLow(Picture img)
    {
        Picture newImg = new Picture(img.getHeight(), img.getWidth());

        newImg.copyPicture(img);

        for(int x = 0; x < newImg.getWidth(); x++ )
        {
            for(int y = 0; y < newImg.getHeight(); y++ )
            {
                clearLow(newImg.getPixel(x,y));
            }
        }
        return newImg;
    }

    /**
     * Clear the lower (rightmost) two bits in a pixel
     * @param p
     */
    public static void clearLow(Pixel p)
    {
        p.setRed(clearCrumb(p.getRed()));
        p.setGreen(clearCrumb(p.getGreen()));
        p.setBlue(clearCrumb(p.getBlue()));
    }
    
    /**
     * Set the lower 2 bits in a pixel with the highest 2 bits in a color
     * @param p
     * @param c
     */
    public static void setLow(Pixel p, Color c)
    {
        clearLow(p)
        p.setRed(p.getRed() + (c.getRed() >> 6));
        p.setGreen(p.getGreen() + (c.getGreen() >> 6));
        p.setBlue(p.getBlue() + (c.getBlue() >> 6));
    }

    /**
     * Gets the lower 2 bits in a pixel and makes it the highest, clearing the rest.
     * @param p
     * @param c
     */
    public static void getLow(Pixel p)
    {
        p.setRed((p.getRed() & 3) >> 6 );
        p.setGreen((p.getGreen() & 3) >> 6);
        p.setBlue((p.getBlue() & 3) >> 6);
    }

    /**
     * Clear the lower (rightmost) two bits in an integer
     * @param i
     */
    public static int clearCrumb(int i)
    {
        return i & -4;
    }
}